# Discourse-Privacy-Policy-Enforce

Simple helper for GDPR requirements

## Settings

* Enable/Disable plugin
* Headline
* Checkbox content
* Name of the `Accept` button
* ID of topic with your privacy policy

## Installation
Clone this repo into your Discourse plugins directory or add this repo to `app.yml`

## Contributing
Interested in helping out? Welcome! Feel free to open issue or pull request :bulb: