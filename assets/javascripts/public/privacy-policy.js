var redirect = JSON.parse(document.documentElement.dataset.config).redirect;

if (redirect) {
  window.location.replace('/');
}


$(document).ready(function() {
  $('#button').click(function() {
    $.ajax({
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '/privacy-policy',
      data: {
        accept: $('#agree').is(":checked")
      },
      success: function (data) {
        if (data.error === false) {
          window.location.replace('/')
        }
      }
    })
  })
});