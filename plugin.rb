# name: discourse-privacy-policy-enforce
# about: Simple helper for GDPR requirements
# version: 1.7
# authors: Kacper Sawicki (kacpersaw)
# url: https://gitlab.com/kacpersaw/discourse-privacy-policy-enforce

enabled_site_setting :privacy_policy_enforce_enabled

add_admin_route 'privacy_policy_enforce.title', 'privacy-policy-enforce'

Rails.application.config.assets.paths << Rails.root.join('plugins', 'discourse-privacy-policy-enforce', 'assets', 'javascripts', 'public')
Rails.application.config.assets.precompile += %w{
  privacy-policy.js
}

after_initialize do
  require_dependency "application_controller"
  require_dependency "plugin_store"

  module ::PrivacyPolicyEnforce
    PLUGIN_NAME = "privacy_policy_enforce".freeze
    class Engine < ::Rails::Engine
      engine_name PrivacyPolicyEnforce::PLUGIN_NAME
      isolate_namespace PrivacyPolicyEnforce
    end
  end

  class PrivacyPolicyEnforce::PrivacyPolicyEnforceCsvController < ::ApplicationController
    requires_plugin PrivacyPolicyEnforce::PLUGIN_NAME
    skip_before_action :check_xhr

    before_action :ensure_logged_in

    def index
      if current_user.admin
        @users = User.all
        users_cf = []

        @users.each do |user|
          u = {
              username: user.username,
              custom_fields: user.custom_fields
          }
          users_cf << u
        end

        attributes = %w{username accepted_privacy_policy_date}
        csv_string = CSV.generate do |csv|
          csv << attributes

          users_cf.each do |user|
            csv << [user[:username], (user[:custom_fields]["accepted_pp_date"] || nil)]
          end
        end

        respond_to do |format|
          format.csv do
            options = {
                :filename => "ppe-users-#{Date.today}.csv",
                :type => 'text/csv'
            }

            send_data(csv_string, options)
          end
        end
      end
    end
  end

  class PrivacyPolicyEnforce::PrivacyPolicyEnforceController < ::ApplicationController
    requires_plugin PrivacyPolicyEnforce::PLUGIN_NAME

    prepend_view_path(Rails.root.join('plugins', 'discourse-privacy-policy-enforce', 'views'))
    layout 'privacy_policy'

    before_action :ensure_logged_in

    helper_method :redirect

    def redirect
      post = Topic.find(SiteSetting.privacy_policy_enforce_topic_id).posts.first
      if current_user.custom_fields["accepted_pp_date"] != nil && post != nil
        if current_user.custom_fields["accepted_pp_date"] > post["updated_at"]
          return true
        end
      end

      return false
    end

    def index
    end

    def accept
      if params["accept"]
        current_user.custom_fields["accepted_pp_date"] = DateTime.now.utc
        current_user.save
        return render :json => {error: false}
      end

      render :json => {error: true}
    end
  end

  PrivacyPolicyEnforce::Engine.routes.draw do
    get "/" => "privacy_policy_enforce#index"
    post "/" => "privacy_policy_enforce#accept"

    get 'users' => 'privacy_policy_enforce_csv#index'
  end

  Discourse::Application.routes.append do
    mount ::PrivacyPolicyEnforce::Engine, at: "/privacy-policy"

    get '/admin/plugins/privacy-policy-enforce' => 'admin/plugins#index', constraints: StaffConstraint.new
  end

  class ::ApplicationController
    before_action :redirect_to_pp_if_required, if: :current_user

    def redirect_to_pp_if_required
      if SiteSetting.privacy_policy_enforce_enabled
        unless request.fullpath.include?(".json") || request.fullpath.include?(".png")
          post = Topic.find(SiteSetting.privacy_policy_enforce_topic_id).posts.first

          if current_user.custom_fields["accepted_pp_date"] == nil ||
              current_user.custom_fields["accepted_pp_date"] < post["updated_at"]

            if request.fullpath != "/privacy-policy/" && request.method != "POST"
              redirect_to "/privacy-policy"
            end
          else
            if request.fullpath == "/privacy-policy/"
              redirect_to "/"
            end
          end
        end
      end
    end
  end

  User.register_custom_field_type('accepted_pp_date', :date)
  add_to_serializer(:current_user, :accepted_pp_date) {object.custom_fields["accepted_pp_date"]}
end